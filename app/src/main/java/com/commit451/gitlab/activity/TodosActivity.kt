package com.commit451.gitlab.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.view.GravityCompat
import com.commit451.gitlab.App
import com.commit451.gitlab.R
import com.commit451.gitlab.adapter.TodoPagerAdapter
import com.commit451.gitlab.data.Prefs
import com.commit451.gitlab.databinding.ActivityTodosBinding
import com.commit451.gitlab.event.CloseDrawerEvent
import org.greenrobot.eventbus.Subscribe

/**
 * Shows the projects
 */
class TodosActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, TodosActivity::class.java)
        }
    }

    private lateinit var binding: ActivityTodosBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Prefs.startingView = Prefs.STARTING_VIEW_TODOS
        binding = ActivityTodosBinding.inflate(layoutInflater)
        setContentView(binding.root)
        App.bus().register(this)

        binding.toolbar.setTitle(R.string.nav_todos)
        binding.toolbar.setNavigationIcon(R.drawable.ic_menu_24dp)
        binding.toolbar.setNavigationOnClickListener { binding.drawerLayout.openDrawer(GravityCompat.START) }
        binding.viewPager.adapter = TodoPagerAdapter(this, supportFragmentManager)
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        App.bus().unregister(this)
    }

    @Suppress("UNUSED_PARAMETER")
    @Subscribe
    fun onEvent(event: CloseDrawerEvent) {
        binding.drawerLayout.closeDrawers()
    }
}
