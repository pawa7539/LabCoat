package com.commit451.gitlab.api

import com.commit451.gitlab.model.Account
import com.commit451.gitlab.model.rss.Entry
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.rxjava3.core.Single
import okhttp3.OkHttpClient
import retrofit2.Response

/**
 * Provides access to all the GitLab things. Wraps RSS and the Retrofit service, in
 * case we need to do overrides or global
 */
class GitLab private constructor(
    builder: Builder,
    gitLabService: GitLabService,
    gitLabRss: GitLabRss
) : GitLabService by gitLabService, GitLabRss by gitLabRss {

    val moshi: Moshi
    val client: OkHttpClient
    val account: Account

    init {
        client = builder.clientBuilder?.build() ?: OkHttpClient()
        account = builder.account
        moshi = MoshiProvider.moshi
    }

    inline fun <reified T> loadAny(url: String): Single<Response<T>> {
        return get(url)
            .map {
                val source = it.body()?.source() ?: throw NullBodyException()
                val body = moshi.adapter(T::class.java).fromJson(source)
                Response.success(body, it.raw())
            }
    }

    inline fun <reified T> loadAnyList(url: String): Single<retrofit2.Response<List<T>>> {
        return get(url)
            .map {
                val source = it.body()?.source() ?: throw NullBodyException()
                val type = Types.newParameterizedType(List::class.java, T::class.java)
                val body = moshi.adapter<List<T>>(type).fromJson(source)
                Response.success(body, it.raw())
            }
    }

    fun feed(url: String): Single<Response<List<Entry>>> {
        return getFeed(url).map { Response.success(it.body()?.entries ?: emptyList(), it.raw()) }
    }

    class Builder(internal val account: Account) {

        internal var clientBuilder: OkHttpClient.Builder? = null

        fun clientBuilder(clientBuilder: OkHttpClient.Builder): Builder {
            this.clientBuilder = clientBuilder
            return this
        }

        fun build(): GitLab {
            val client = clientBuilder?.build() ?: OkHttpClient()
            val gitLabService = GitLabFactory.create(account, client)
            val gitLabRss = GitLabRssFactory.create(account, client)
            return GitLab(this, gitLabService, gitLabRss)
        }
    }

}
