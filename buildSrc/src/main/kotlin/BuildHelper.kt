import org.gradle.api.Project
import java.io.File

@Suppress("MemberVisibilityCanBePrivate")
object BuildHelper {

    private var locatedFile: Boolean? = null

    fun appVersionName(): String {
        return "2.7.9"
    }

    fun appVersionCode(): Int {
        val parts = appVersionName().split(".")
        val versionMajor = parts[0].toInt()
        val versionMinor = parts[1].toInt()
        val versionPatch = parts[2].toInt()
        // this is something I got from u2020 a while back... meh
        return versionMajor * 1000000 + versionMinor * 10000 + versionPatch * 100
    }

    fun keystoreFile(project: Project): File {
        return project.file("${project.rootDir}/app/${project.propertyOrEmpty("KEYSTORE_NAME")}")
    }
}
